# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'shpobs_Step2.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_shpobsStep2(object):
    def setupUi(self, shpobsStep2):
        shpobsStep2.setObjectName("shpobsStep2")
        shpobsStep2.resize(639, 448)
        shpobsStep2.setWindowTitle("Shp to Obs Converter")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        shpobsStep2.setWindowIcon(icon)
        shpobsStep2.setAccessibleName("")
        self.Convert = QtWidgets.QPushButton(shpobsStep2)
        self.Convert.setGeometry(QtCore.QRect(530, 410, 98, 27))
        self.Convert.setDefault(True)
        self.Convert.setObjectName("Convert")
        self.Cancel = QtWidgets.QPushButton(shpobsStep2)
        self.Cancel.setGeometry(QtCore.QRect(320, 410, 98, 27))
        self.Cancel.setObjectName("Cancel")
        self.line = QtWidgets.QFrame(shpobsStep2)
        self.line.setGeometry(QtCore.QRect(10, 390, 611, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.Tabs = QtWidgets.QTabWidget(shpobsStep2)
        self.Tabs.setGeometry(QtCore.QRect(10, 10, 611, 381))
        self.Tabs.setObjectName("Tabs")
        self.BuildingsTab = QtWidgets.QWidget()
        self.BuildingsTab.setObjectName("BuildingsTab")
        self.Build_1 = QtWidgets.QLabel(self.BuildingsTab)
        self.Build_1.setGeometry(QtCore.QRect(10, 10, 581, 21))
        self.Build_1.setObjectName("Build_1")
        self.Build_2 = QtWidgets.QLabel(self.BuildingsTab)
        self.Build_2.setGeometry(QtCore.QRect(10, 80, 581, 21))
        self.Build_2.setObjectName("Build_2")
        self.Sel_Build_Layer = gui.QgsMapLayerComboBox(self.BuildingsTab)
        self.Sel_Build_Layer.setGeometry(QtCore.QRect(10, 40, 221, 27))
        self.Sel_Build_Layer.setObjectName("Sel_Build_Layer")
        self.Sel_buildhight_field = gui.QgsFieldComboBox(self.BuildingsTab)
        self.Sel_buildhight_field.setGeometry(QtCore.QRect(10, 110, 221, 27))
        self.Sel_buildhight_field.setObjectName("Sel_buildhight_field")
        self.use3dCheckBox = QtWidgets.QCheckBox(self.BuildingsTab)
        self.use3dCheckBox.setEnabled(False)
        self.use3dCheckBox.setGeometry(QtCore.QRect(260, 110, 321, 23))
        self.use3dCheckBox.setObjectName("use3dCheckBox")
        self.Tabs.addTab(self.BuildingsTab, "")
        self.TreesTab = QtWidgets.QWidget()
        self.TreesTab.setObjectName("TreesTab")
        self.Label_Veg_Layer = QtWidgets.QLabel(self.TreesTab)
        self.Label_Veg_Layer.setGeometry(QtCore.QRect(20, 20, 581, 21))
        self.Label_Veg_Layer.setObjectName("Label_Veg_Layer")
        self.treesconf = QtWidgets.QGroupBox(self.TreesTab)
        self.treesconf.setEnabled(False)
        self.treesconf.setGeometry(QtCore.QRect(10, 100, 581, 201))
        self.treesconf.setObjectName("treesconf")
        self.treehightlabel = QtWidgets.QLabel(self.treesconf)
        self.treehightlabel.setGeometry(QtCore.QRect(10, 20, 181, 51))
        self.treehightlabel.setObjectName("treehightlabel")
        self.treecrownlabel = QtWidgets.QLabel(self.treesconf)
        self.treecrownlabel.setGeometry(QtCore.QRect(200, 20, 191, 51))
        self.treecrownlabel.setObjectName("treecrownlabel")
        self.trunchightlabel = QtWidgets.QLabel(self.treesconf)
        self.trunchightlabel.setGeometry(QtCore.QRect(10, 120, 181, 51))
        self.trunchightlabel.setObjectName("trunchightlabel")
        self.Select_Crown_Expan_4 = QtWidgets.QLabel(self.treesconf)
        self.Select_Crown_Expan_4.setGeometry(QtCore.QRect(200, 120, 181, 51))
        self.Select_Crown_Expan_4.setObjectName("Select_Crown_Expan_4")
        self.treetypelabel = QtWidgets.QLabel(self.treesconf)
        self.treetypelabel.setGeometry(QtCore.QRect(400, 20, 191, 51))
        self.treetypelabel.setObjectName("treetypelabel")
        self.Sel_treehight_field = gui.QgsFieldComboBox(self.treesconf)
        self.Sel_treehight_field.setGeometry(QtCore.QRect(10, 70, 161, 27))
        self.Sel_treehight_field.setObjectName("Sel_treehight_field")
        self.Sel_crownexp_field = gui.QgsFieldComboBox(self.treesconf)
        self.Sel_crownexp_field.setGeometry(QtCore.QRect(200, 70, 161, 27))
        self.Sel_crownexp_field.setObjectName("Sel_crownexp_field")
        self.Sel_treetype_field = gui.QgsFieldComboBox(self.treesconf)
        self.Sel_treetype_field.setGeometry(QtCore.QRect(400, 70, 161, 27))
        self.Sel_treetype_field.setObjectName("Sel_treetype_field")
        self.Sel_trunchight_field = gui.QgsFieldComboBox(self.treesconf)
        self.Sel_trunchight_field.setGeometry(QtCore.QRect(10, 170, 161, 27))
        self.Sel_trunchight_field.setObjectName("Sel_trunchight_field")
        self.Sel_truncrad_field = gui.QgsFieldComboBox(self.treesconf)
        self.Sel_truncrad_field.setGeometry(QtCore.QRect(200, 170, 161, 27))
        self.Sel_truncrad_field.setObjectName("Sel_truncrad_field")
        self.Sel_Tree_Layer = gui.QgsMapLayerComboBox(self.TreesTab)
        self.Sel_Tree_Layer.setGeometry(QtCore.QRect(20, 50, 160, 27))
        self.Sel_Tree_Layer.setCurrentText("")
        self.Sel_Tree_Layer.setObjectName("Sel_Tree_Layer")
        self.Tabs.addTab(self.TreesTab, "")
        self.CenterTab = QtWidgets.QWidget()
        self.CenterTab.setObjectName("CenterTab")
        self.centerpointslabel = QtWidgets.QLabel(self.CenterTab)
        self.centerpointslabel.setGeometry(QtCore.QRect(20, 20, 581, 21))
        self.centerpointslabel.setObjectName("centerpointslabel")
        self.centerpointslabel_2 = QtWidgets.QLabel(self.CenterTab)
        self.centerpointslabel_2.setGeometry(QtCore.QRect(20, 110, 581, 21))
        self.centerpointslabel_2.setObjectName("centerpointslabel_2")
        self.Sel_centerpoints_layer = gui.QgsMapLayerComboBox(self.CenterTab)
        self.Sel_centerpoints_layer.setGeometry(QtCore.QRect(20, 60, 291, 27))
        self.Sel_centerpoints_layer.setObjectName("Sel_centerpoints_layer")
        self.Sel_centerpoints_field = gui.QgsFieldComboBox(self.CenterTab)
        self.Sel_centerpoints_field.setGeometry(QtCore.QRect(20, 150, 291, 27))
        self.Sel_centerpoints_field.setObjectName("Sel_centerpoints_field")
        self.Tabs.addTab(self.CenterTab, "")
        self.OutTab = QtWidgets.QWidget()
        self.OutTab.setObjectName("OutTab")
        self.output_intro = QtWidgets.QLabel(self.OutTab)
        self.output_intro.setGeometry(QtCore.QRect(20, 20, 561, 51))
        self.output_intro.setObjectName("output_intro")
        self.out_dir_label = QtWidgets.QLabel(self.OutTab)
        self.out_dir_label.setGeometry(QtCore.QRect(20, 80, 191, 17))
        self.out_dir_label.setObjectName("out_dir_label")
        self.outfilelabel = QtWidgets.QLabel(self.OutTab)
        self.outfilelabel.setGeometry(QtCore.QRect(20, 140, 66, 17))
        self.outfilelabel.setObjectName("outfilelabel")
        self.outdir = QtWidgets.QLineEdit(self.OutTab)
        self.outdir.setGeometry(QtCore.QRect(20, 100, 411, 27))
        self.outdir.setObjectName("outdir")
        self.outfile = QtWidgets.QLineEdit(self.OutTab)
        self.outfile.setGeometry(QtCore.QRect(20, 160, 451, 27))
        self.outfile.setObjectName("outfile")
        self.outdirFile = gui.QgsFileWidget(self.OutTab)
        self.outdirFile.setGeometry(QtCore.QRect(20, 210, 451, 27))
        self.outdirFile.setFullUrl(True)
        self.outdirFile.setObjectName("outdirFile")
        self.selectOutDirButton = QtWidgets.QPushButton(self.OutTab)
        self.selectOutDirButton.setGeometry(QtCore.QRect(440, 100, 31, 25))
        self.selectOutDirButton.setObjectName("selectOutDirButton")
        self.Tabs.addTab(self.OutTab, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.obsFileNameField = gui.QgsFileWidget(self.tab)
        self.obsFileNameField.setGeometry(QtCore.QRect(30, 90, 551, 39))
        self.obsFileNameField.setObjectName("obsFileNameField")
        self.label = QtWidgets.QLabel(self.tab)
        self.label.setGeometry(QtCore.QRect(30, 70, 391, 17))
        self.label.setObjectName("label")
        self.AddObsAsLayerButton = QtWidgets.QPushButton(self.tab)
        self.AddObsAsLayerButton.setGeometry(QtCore.QRect(398, 310, 191, 29))
        self.AddObsAsLayerButton.setObjectName("AddObsAsLayerButton")
        self.CenterPointDefinitionBox = QtWidgets.QGroupBox(self.tab)
        self.CenterPointDefinitionBox.setGeometry(QtCore.QRect(10, 140, 571, 151))
        self.CenterPointDefinitionBox.setObjectName("CenterPointDefinitionBox")
        self.latBox = QtWidgets.QDoubleSpinBox(self.CenterPointDefinitionBox)
        self.latBox.setGeometry(QtCore.QRect(110, 70, 451, 33))
        self.latBox.setMaximum(9999999999.99)
        self.latBox.setObjectName("latBox")
        self.lonBox = QtWidgets.QDoubleSpinBox(self.CenterPointDefinitionBox)
        self.lonBox.setGeometry(QtCore.QRect(110, 110, 451, 33))
        self.lonBox.setMaximum(999999999.99)
        self.lonBox.setObjectName("lonBox")
        self.label_2 = QtWidgets.QLabel(self.CenterPointDefinitionBox)
        self.label_2.setGeometry(QtCore.QRect(10, 30, 121, 17))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.CenterPointDefinitionBox)
        self.label_3.setGeometry(QtCore.QRect(10, 70, 121, 17))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.CenterPointDefinitionBox)
        self.label_4.setGeometry(QtCore.QRect(10, 110, 81, 17))
        self.label_4.setObjectName("label_4")
        self.projectionSettingsBox = gui.QgsProjectionSelectionWidget(self.CenterPointDefinitionBox)
        self.projectionSettingsBox.setGeometry(QtCore.QRect(110, 30, 451, 27))
        self.projectionSettingsBox.setObjectName("projectionSettingsBox")
        self.Tabs.addTab(self.tab, "")
        self.Help = QtWidgets.QPushButton(shpobsStep2)
        self.Help.setGeometry(QtCore.QRect(430, 410, 98, 27))
        self.Help.setObjectName("Help")

        self.retranslateUi(shpobsStep2)
        self.Tabs.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(shpobsStep2)
        shpobsStep2.setTabOrder(self.Convert, self.Cancel)
        shpobsStep2.setTabOrder(self.Cancel, self.Tabs)
        shpobsStep2.setTabOrder(self.Tabs, self.Sel_treehight_field)
        shpobsStep2.setTabOrder(self.Sel_treehight_field, self.Sel_crownexp_field)
        shpobsStep2.setTabOrder(self.Sel_crownexp_field, self.Sel_treetype_field)
        shpobsStep2.setTabOrder(self.Sel_treetype_field, self.Sel_trunchight_field)
        shpobsStep2.setTabOrder(self.Sel_trunchight_field, self.Sel_truncrad_field)
        shpobsStep2.setTabOrder(self.Sel_truncrad_field, self.outdir)
        shpobsStep2.setTabOrder(self.outdir, self.outfile)

    def retranslateUi(self, shpobsStep2):
        _translate = QtCore.QCoreApplication.translate
        self.Convert.setText(_translate("shpobsStep2", "Convert"))
        self.Cancel.setText(_translate("shpobsStep2", "Cancel"))
        self.Build_1.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose your layer for the buildings input:</p></body></html>"))
        self.Build_2.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for the buildings height:</p></body></html>"))
        self.use3dCheckBox.setText(_translate("shpobsStep2", "get building height from layers z-coodinate"))
        self.Tabs.setTabText(self.Tabs.indexOf(self.BuildingsTab), _translate("shpobsStep2", "Buildings"))
        self.Label_Veg_Layer.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose your layer for the tree input (optional):</p></body></html>"))
        self.treesconf.setTitle(_translate("shpobsStep2", "Trees config"))
        self.treehightlabel.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for<br>the tree height:</p></body></html>"))
        self.treecrownlabel.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for<br>the radius of the tree crown:</p></body></html>"))
        self.trunchightlabel.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for<br>the trunc hight:</p></body></html>"))
        self.Select_Crown_Expan_4.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for<br>the raduis of the trunc:</p></body></html>"))
        self.treetypelabel.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for<br>the tree type:</p></body></html>"))
        self.Tabs.setTabText(self.Tabs.indexOf(self.TreesTab), _translate("shpobsStep2", "Trees (optional)"))
        self.centerpointslabel.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose your layer for the center points:</p></body></html>"))
        self.centerpointslabel_2.setText(_translate("shpobsStep2", "<html><head/><body><p>Please choose the field for the center points elevation:</p></body></html>"))
        self.Tabs.setTabText(self.Tabs.indexOf(self.CenterTab), _translate("shpobsStep2", "Center Point(s)"))
        self.output_intro.setText(_translate("shpobsStep2", "Please name output directory and filename <br>\n"
"(the script will add a number after the filename if there are more then one <br>center point)"))
        self.out_dir_label.setText(_translate("shpobsStep2", "Output File and Directory:"))
        self.outfilelabel.setText(_translate("shpobsStep2", "File name:"))
        self.outdir.setText(_translate("shpobsStep2", "D:\\data\\test\\"))
        self.outfile.setText(_translate("shpobsStep2", "PdaS_Test"))
        self.outdirFile.setFilter(_translate("shpobsStep2", "*.obs"))
        self.selectOutDirButton.setText(_translate("shpobsStep2", "…"))
        self.Tabs.setTabText(self.Tabs.indexOf(self.OutTab), _translate("shpobsStep2", "Output Directory"))
        self.obsFileNameField.setFilter(_translate("shpobsStep2", "*.obs"))
        self.label.setText(_translate("shpobsStep2", "Select a pre-existing .obs file for the display in QGIS:"))
        self.AddObsAsLayerButton.setText(_translate("shpobsStep2", "Add to QGIS"))
        self.CenterPointDefinitionBox.setTitle(_translate("shpobsStep2", "Center Point Settings"))
        self.label_2.setText(_translate("shpobsStep2", "Projection"))
        self.label_3.setText(_translate("shpobsStep2", "X / Easting"))
        self.label_4.setText(_translate("shpobsStep2", "Y / Northing"))
        self.Tabs.setTabText(self.Tabs.indexOf(self.tab), _translate("shpobsStep2", "View in QGIS"))
        self.Help.setText(_translate("shpobsStep2", "Help"))

from qgis import gui

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    shpobsStep2 = QtWidgets.QDialog()
    ui = Ui_shpobsStep2()
    ui.setupUi(shpobsStep2)
    shpobsStep2.show()
    sys.exit(app.exec_())

