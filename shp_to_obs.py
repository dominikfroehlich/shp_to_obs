# from __future__ import absolute_import
# """
# /***************************************************************************
# Name                 : shptoobs
# Description          : Creates a RayMan input file from shp. 
# Date                 : 23/Okt/2018
# copyright            : (C) 2018 by Tamas Gal, Dominik Froehlich
# email                : dominik.froehlich@mailbox.org 
#  ***************************************************************************/
# """

from builtins import str
from builtins import range
from builtins import object
from qgis.utils import showPluginHelp
from PyQt5.QtCore import *
from PyQt5.QtGui import (QIcon)
from PyQt5.QtWidgets import (QAction)
from qgis.PyQt.QtWidgets import (QDialog, QFileDialog, QMessageBox, QProgressBar)
from qgis.core import (Qgis, QgsGeometry, QgsProject, QgsPoint, QgsPointXY, 
                       QgsPolygon, QgsFieldProxyModel, QgsVectorLayer, QgsField, 
                       QgsFeature, QgsMapLayerProxyModel, QgsWkbTypes)
from math import (ceil, floor)
from os import path
import csv
import random

# Initialize Qt resources from file resources.py from local dir
from . import resources

# Import the code for the forms
from .shpobs_Step1 import Ui_shpobsStep1
from .shpobs_Step2 import Ui_shpobsStep2

class ShowDialog1(QDialog, Ui_shpobsStep1): 
    def __init__(self): 
        QDialog.__init__(self)
        self.setupUi(self) 

class ShowDialog2(QDialog, Ui_shpobsStep2): 
    def __init__(self): 
        QDialog.__init__(self)
        self.setupUi(self) 

class shp_to_obs(object):
  
  showUnsuppostedGeometryType = True
  buildLayerIs3d = False
  
  
  def __init__(self, iface):
    # Save reference to the QGIS interface
    self.iface = iface
    self.canvas = self.iface.mapCanvas()
  
  
  # Create action that will start plugin configuration
  def initGui(self):
    
    self.action = QAction(QIcon(":/plugins/shp_to_obs/icon.png"), \
        "shp_to_obs", self.iface.mainWindow())
    
    # Vector menu and toolbar available
    self.iface.addVectorToolBarIcon(self.action)
    self.iface.addPluginToVectorMenu("&shp_to_obs", self.action)
    self.action.triggered.connect(self.run)
  
  
  # Remove buttons from menu and toolbar
  def unload(self):
    
    self.iface.removePluginVectorMenu("&shp_to_obs", self.action)
    self.iface.removeVectorToolBarIcon(self.action)

  
  # Modify "convert" button caption and connection according to selected tab
  def guiTabIndexChanged(self, i):
   
   if i == 4:
      _mainDialog.Convert.setText('Add Layer')
      _mainDialog.Convert.clicked.disconnect()
      _mainDialog.Convert.clicked.connect(self.generateObsLayers)
   else:
      _mainDialog.Convert.setText('Convert')
      _mainDialog.Convert.clicked.disconnect()
      _mainDialog.Convert.clicked.connect(self.generateObstacleFile)
  
  
  # Returns fields for the given layer
  def getFields(self, lyr):
    
    # get the layers dataProvider
    dp = lyr.dataProvider()
    
    # get the fileds from the dataProvider
    fnm = dp.fieldNameMap()
    
    # create a list of the fields
    lst = []
    for key in fnm:
      lst.append(key)
    
    # sort ascending
    lst.sort()
    
    # return the sorted fields list
    return lst
  
  
  # def extract_attribute(self, layer, field_index): 
  #    if (layer.isValid() and field_index >= 0):
  #       features = layer.getFeatures()
  #       attr_list = []
  #       # calculate field index
  #       field_list = self.getFields(layer)
  #       fieldName = str(field_list[field_index])
  #       for feature in features:
  #         try:
  #           attr_list.append(float(feature[fieldName])) 
  #         except:
  #           attr_list.append(str(feature[fieldName]).lower())
  #       return attr_list
  #    else:
  #       QMessageBox.critical(None, "Attribute extraction faild", str("Invalid layer or field index: Layer = " + str(layer.name()) + " Field Nr. = " + str(field_index)))
  
  
  # Get one column of the attribute table by layer and fieldName, returns a list
  def extractAttributeByName(self, layer, fieldName): 
     
     # check if the specified layer is valid and the fieldName exists in the attribute table, fails with error message if otherwise
     if (layer.isValid() and fieldName is not None):
        
        # get the features (Qgs class) from the layer
        features = layer.getFeatures()
        
        # create an empty list object to append the attributes
        attributesList = []
        
        # append the attribute for any feature 
        for feature in features:
          try:
            attributesList.append(float(feature[fieldName])) 
          except:
            attributesList.append(str(feature[fieldName]).lower())
        
        # return the attributes list
        return attributesList
     
     # Throw error messageBox in case layer or fieldName are invalid / don't exist
     else:
        QMessageBox.critical(None, "Attribute extraction faild", str("Invalid layer or field index: Layer = " + str(layer.name()) + " Field Name = " + fieldName))
  
  
  # Extract 2D coordinates from a point geometry layer, used for trees and centerpoints only
  def extractPointCoordinates(self, layer):
     
     # check if layer is valid. Return otherwise.
     if not layer.isValid():
       return([], True)
     
     # get the layers features
     features = layer.getFeatures()
     
     # create an empty list for the results 
     pointGeometriesList = []
     invalid = 0
     abort = False
     
     # append the geometries for any valid point feature (convert to point if other)
     for feature in features:
       if feature.isValid():
         geom = feature.geometry().asPoint()
         pointGeometriesList.append(geom)
       
       # count invalid features
       else:
         invalid = invalid + 1
         continue
     
     # throw an error message box if invalid features present and set abort flag
     if (invalid > 0):
       QMessageBox.critical(None, "Reading point geometry faild!", str("Reading out point geometry faild due to " + str(invalid) + " invalid features!"))
       abort = True
     
     # return the list of points together with the abort flag
     return(pointGeometriesList, abort)
  
  
  # Extract the polygon geometries from layer "layer"
  def extractPolygonGeometries(self, layer): 
     
     # check if layer is valid. Return immediatelly otherwise.
     if not layer.isValid():
       QMessageBox.critical(None, "Check selected layer!", ":-/ Faild to load buildings layer! (Extract polygon geometry routine):-/" )
       return []
     
     # get the features from "layer"
     features = layer.getFeatures()
     geometriesList = []
     invalid = 0
     
     # iterate over all features, append the valid ones as geometries and increase invalid counter otherwise
     for feature in features:
       if feature.isValid():
         geometriesList.append( QgsGeometry( feature.geometry() ) )
       else:
         invalid = invalid + 1
     
     # show error bo in case of invalid polygons
     if (invalid > 0):
       QMessageBox.critical(None, "Check selected layer!", str("Your buildings layer contains " + str(invalid) + "invalid features!"))
     
     # return the geometries
     return geometriesList
  
  
  # Handle click event for selectOutFileButton
  def on_selectOutFileButtonButton_clicked(self):
     
     # show saveAs dialog with filter for .obs
     qFileDialogProperties = QFileDialog.getSaveFileName(parent=None, caption="Select .obs output file name", filter="RyMan Obstacle Files (*.obs)")
     
     # get path and filename from the dialog's properties
     fileName = qFileDialogProperties[0]
     
     # split the filepath/name string at any "."
     split = fileName.split('.')
     
     # remove the trailing "obs" if present
     if len(split) > 0 and split[-1] == 'obs':  # remove ending for now
         fileName = split[0:-1]
     
     # show the file path and name in the GUI
     _mainDialog.outdir.setText(''.join(fileName) + ".obs")
  
  
  # Extract the file path and name of the current project file
  def getProjectFilePathAndName(self): 
        
        # get the current QGIS project file (if any)
        projectFile = QFileInfo(QgsProject.instance().fileName())
        
        # create a list to hold the file path and the file name
        pathAndNamePartsList = []
        
        # if there is a projectFile
        if projectFile:
          
          # append the file path
          pathAndNamePartsList.append(str(projectFile.absolutePath()))
          
          # get the file name
          filename = str(projectFile.fileName())
          
          # remove the filenames extension (if present) and append
          if filename.count(".") > 0:
             pathAndNamePartsList.append(filename.split(".")[0])
          else:
             pathAndNamePartsList.append(filename)
          pathAndNamePartsList.append(projectFile.fileName())
        
        # enter defaults if there is not project file loaded
        else:
          pathAndNamePartsList.append(r'C:\Users')
          pathAndNamePartsList.append(r'test')
        
        # return filepath and name
        return pathAndNamePartsList
  
  
  # Refresh height field dropdown and check enabling when new buildings layer is selected
  def buildLayerIndexChanged(self):
     
     # set new buildings layer
     buildingsLayer = _mainDialog.Sel_Build_Layer.currentLayer()
     
     # check if the layer is three dimensional
     buildLayer3d = QgsWkbTypes.hasZ(buildingsLayer.wkbType())
     
     # update height field dropdown menu
     _mainDialog.Sel_buildhight_field.setLayer(buildingsLayer)
     
     # enable/disable the field dropdown and the 3d checkbox
     if buildLayer3d:
        # _mainDialog.Sel_buildhight_field.setEnabled(False)
        _mainDialog.use3dCheckBox.setEnabled(True)
        _mainDialog.use3dCheckBox.setChecked(True)
        self.buildLayerIs3d = True
     else:
        _mainDialog.Sel_buildhight_field.setEnabled(True)
        _mainDialog.use3dCheckBox.setChecked(False)
        _mainDialog.use3dCheckBox.setEnabled(False)
  
  
  # handle the z-coordinate usage checkbox checked events
  def buildLayer3dCheckboxChecked(self, state):
     
     # set flag for buildings layer height handeling
     self.buildLayerIs3d = (state == Qt.Checked)
     
     # adjust enabeling of the height fiueld selector dropdown
     if self.buildLayerIs3d:
        _mainDialog.Sel_buildhight_field.setEnabled(False)
     else:
        _mainDialog.Sel_buildhight_field.setEnabled(True)
  
  
  # Update the dropdown elements when new tree layer is selected
  def treeLayerIndexChanged(self):
     
     # enable the dropdown elements
     _mainDialog.treesconf.setEnabled(True)
     
     # ge the new tree layer
     treesLayer = _mainDialog.Sel_Tree_Layer.currentLayer()
     
     # set the new tree layer as source for the QgsFieldSelect dropdown elements
     _mainDialog.Sel_treehight_field.setLayer(treesLayer)
     _mainDialog.Sel_crownexp_field.setLayer(treesLayer)
     _mainDialog.Sel_treetype_field.setLayer(treesLayer)
     _mainDialog.Sel_trunchight_field.setLayer(treesLayer)
     _mainDialog.Sel_truncrad_field.setLayer(treesLayer)
  
  
  # Update the vertical offset dropdown when new center points layer is selected
  def centerpointsLayerIndexChanged(self):
     
     _mainDialog.Sel_centerpoints_field.setLayer(_mainDialog.Sel_centerpoints_layer.currentLayer())
  
  
  def deleteDoubleVertices(self, geometryVertices):
    validPoints = []
    validPoints.append(geometryVertices[0])  # first point is always valid!
    for i in range(len(geometryVertices) - 1):
      p1 = geometryVertices[i + 1]
      p2 = geometryVertices[i]
      if (not ((p1.x() == p2.x()) and (p1.y() == p2.y()))):
        validPoints.append(p1)  # only copy next point if not the same as before
    return validPoints
  
  
  def find_closestVertices(self, sehalfNumPoints, sehalfNumPointsCeil):
    p1 = sehalfNumPoints[0]
    p2 = sehalfNumPointsCeil[0]
    vertnums = []
    vertnums.append(0)  # vertex number 1
    vertnums.append(0)  # vertex number 2
    pdist = abs(p1.x() - p2.x()) + abs(p1.y() + p2.y())
    for i in range(len(sehalfNumPoints)):
      p1 = sehalfNumPoints[i]
      for j in range(len(sehalfNumPointsCeil)):
        p2 = sehalfNumPointsCeil[j]
        temp_pdist = abs(p1.x() - p2.x()) + abs(p1.y() + p2.y())
        if(temp_pdist < pdist):
          vertnums[0] = i
          vertnums[1] = j
          pdist = temp_pdist
    return(vertnums)
  
  
  def insert_ring(self, r, insert_at, mainset, ringset):
    ringset = self.deleteDoubleVertices(ringset)  # delete double vertices from inner ring
    p1 = ringset[0]
    p2 = ringset[-1]
    if (((p1.x() == p2.x()) and (p1.y() == p2.y()))):  # make sure the first and last point of the ringet are not the same!
      del ringset[-1]
    # odd = floor(r / 2.0) < (r / 2.0)  # ring has odd number? Then is inner one. Insert reverse!
    newPoints = []
    mainp = len(mainset)  # number of points in main set
    ringp = len(ringset)  # number of points in ring to insert
    for i in range(insert_at[0] + 1):  # copy all points bevore
      newPoints.append(mainset[i])
    for j in range(ringp):
      ring_index = insert_at[1] + j  # or not invert at all?
      if(ring_index >= ringp):
        ring_index = ring_index - ringp
      newPoints.append(ringset[ring_index])
    newPoints.append(ringset[insert_at[1]])  # repeat first ring point to close ring
    newPoints.append(mainset[insert_at[0]])  # repeat insert point of main set to integrate the ring
    for k in range(mainp - 1 - insert_at[0]):  # copy rest of original set
      # target_index = insert_at[0] + ringp + 3 + k
      source_index = insert_at[0] + 1 + k
      newPoints.append(mainset[source_index])
    return(newPoints)
  
  
  # create an obstacle file row for a building-type obstacle
  # The method supports up to 4 vertices (as does the obs specification), while the last one may be None-type
  def pointsToBuildingObstacleRow(self, point1, point2, point3, point4, roofHeight, cx, cy, ch):
      
      # determine building height
      p1z = roofHeight  # init with z coordinate from attributes
      p2z = roofHeight
      p3z = roofHeight
      
      # overwrite by z-coordinate if desired
      if self.buildLayerIs3d:
         point1 = QgsPoint(point1)  # make sure there is a z-coordinate
         p1z = point1.z()
         point2 = QgsPoint(point2)
         p2z = point2.z()
         point3 = QgsPoint(point3)
         p3z = point3.z()
      
      # init point4 as between points 1 and 3
      p4x = point3.x() + ((point1.x() - point3.x()) / 2.0)
      p4y = point3.y() + ((point1.y() - point3.y()) / 2.0)
      p4z = p3z + p1z / 2.0
      
      # overwrite if point4 is specified
      if point4 != None:
        p4x = point4.x()
        p4y = point4.y()
        p4z = roofHeight
        if self.buildLayerIs3d:
          point4 = QgsPoint(point4)
          p4z = point4.z()
      
      np1x = '%.2f' % round(point1.x() - cx, 2)  # calculates coordinates from those of point and center points (x-dir)
      np1y = '%.2f' % round(point1.y() - cy, 2)  # y dir
      np1z = '%.2f' % round(p1z - ch, 2)  # z dir
      
      np2x = '%.2f' % round(point2.x() - cx, 2)  # resp
      np2y = '%.2f' % round(point2.y() - cy, 2)
      np2z = '%.2f' % round(p2z - ch, 2)
       
      np3x = '%.2f' % round(point3.x() - cx, 2)
      np3y = '%.2f' % round(point3.y() - cy, 2)
      np3z = '%.2f' % round(p3z - ch, 2)
      
      np4x = '%.2f' % round(p4x - cx, 2)
      np4y = '%.2f' % round(p4y - cy, 2)
      np4z = '%.2f' % round(p4z - ch, 2)
      
      np5x = '%.2f' % round(point1.x() - cx, 2)  # bottom points
      np5y = '%.2f' % round(point1.y() - cy, 2)
      np5z = '%.2f' % round(0, 2)
      
      np6x = '%.2f' % round(point2.x() - cx, 2)
      np6y = '%.2f' % round(point2.y() - cy, 2)
      np6z = '%.2f' % round(0, 2)
      
      np7x = '%.2f' % round(point3.x() - cx, 2)
      np7y = '%.2f' % round(point3.y() - cy, 2)
      np7z = '%.2f' % round(0, 2)
      
      np8x = '%.2f' % round(p4x - cx, 2)
      np8y = '%.2f' % round(p4y - cy, 2)
      np8z = '%.2f' % round(0, 2)
      
      line = ["g", str(np1x), str(np1y), str(np1z), str(np2x), str(np2y), str(np2z), str(np3x), str(np3y), str(np3z), str(np4x), str(np4y), str(np4z), str(np5x), str(np5y), str(np5z), str(np6x), str(np6y), str(np6z), str(np7x), str(np7y), str(np7z), str(np8x), str(np8y), str(np8z), "0.30", "0.97"]
      return(line)
  
  
  # extracts the vertices from a polygon geometry
  # does consider a workarpound for "as(Multi)Polygon" drops z-coordinate unintendedly
  # workaround is horribly inefficient, replace as soon as generic "as(Multi)Polygon" is fixed!
  def extractVerticesFromPolygon(self, buildingGeometry):
      
      # create empty geometries list to return
      geometryVertices = []
      
      # get geometry type
      wkbType = buildingGeometry.type()
      
      if not wkbType == QgsWkbTypes.PolygonGeometry:
        
        # if wrong geometry type show warning, but only once!
        if(self.showUnsuppostedGeometryType):
          QMessageBox.critical(None, "DEBUG:", str("Building is of unsupported geometry type " + str(wkbType) + ". Further errors of the same type will not be displayed." ))
          self.showUnsuppostedGeometryType = False
          
        # return empty list
        return([])
      
      # count current vertex, workaround for asPolygon will drop z-coordinate
      vertexNum = 0
      
      # if geometry is individual polygons (independent from z or m coordinates)
      if not buildingGeometry.isMultipart():
        currentPolygon = buildingGeometry.asPolygon()
        
        if(len(currentPolygon) > 1):  # if the geometry contains more than one ring
          for p in range(len(currentPolygon[0])): # iterate over the first ring and copy points to geometryVertices
            # geometryVertices.append(currentPolygon[0][p]) # list with point of vertexes. Like [(3.41399e+06,5.3181e+06), (3.41398e+06,5.31808e+06)]
            geometryVertices.append(buildingGeometry.vertexAt(vertexNum))
            vertexNum = vertexNum + 1
          for r in range(1, len(currentPolygon)):
            newring = []  # empty container to store items of the new ring temporarily
            for p in range(len(currentPolygon[r])): # iterate over the rings and copy point coordinates
              # newring.append(currentPolygon[r][p]) # list with point of vertexes. Like [(3.41399e+06,5.3181e+06), (3.41398e+06,5.31808e+06)]
              newring.append(buildingGeometry.vertexAt(vertexNum))
              vertexNum = vertexNum + 1
            insert_at = self.find_closestVertices(geometryVertices, newring)
            # geometryVertices = self.insert_ring(r, insert_at, geometryVertices, newring)
            geometryVertices.append(buildingGeometry.vertexAt(vertexNum))
            vertexNum = vertexNum + 1
        else:
          for p in range(len(currentPolygon[0])):   # only for 1st geometry of only one present
            # geometryVertices.append(currentPolygon[0][p]) # list with point of vertexes.
            geometryVertices.append(buildingGeometry.vertexAt(vertexNum))
            vertexNum = vertexNum + 1
      
      else:
        # if MultiPolygons
        currentPolygon = buildingGeometry.asMultiPolygon()
        for pol in range(len(currentPolygon)): # iterate over the polygons of the MultiPolygon
          for p in range(len(currentPolygon[pol][0])): # iterate over the first ring and copy points to geometryVertices
            # geometryVertices.append(currentPolygon[pol][0][p]) # list with point of vertexes. Like [(3.41399e+06,5.3181e+06), (3.41398e+06,5.31808e+06)]
            geometryVertices.append(buildingGeometry.vertexAt(vertexNum))
            vertexNum = vertexNum + 1
          for r in range(len(currentPolygon[pol])): # iterate over the rings of the polygon
            newring = []  # empty container to store items of the new ring temporarily
            for p in range(1, len(currentPolygon[pol][r])):  # iterate over the points of the ring
              # newring.append(currentPolygon[pol][r][p]) # list with point of vertexes. Like [(3.41399e+06,5.3181e+06), (3.41398e+06,5.31808e+06)]
              newring.append(buildingGeometry.vertexAt(vertexNum))
              vertexNum = vertexNum + 1
            insert_at = self.find_closestVertices(geometryVertices, newring)
            # geometryVertices = self.insert_ring(r, insert_at, geometryVertices, newring)
            geometryVertices.append(buildingGeometry.vertexAt(vertexNum))
            vertexNum = vertexNum + 1
            
      
      geometryVertices = self.deleteDoubleVertices(geometryVertices)  # delete vertices with same position
      return(geometryVertices)
  
  
  def shwhelp(self):
     showPluginHelp()
  
  
  def generateObstacleFile(self):
    
    # Read layers
    buildingsLayer = _mainDialog.Sel_Build_Layer.currentLayer()
    treesLayer = _mainDialog.Sel_Tree_Layer.currentLayer()
    centerPointsLayer = _mainDialog.Sel_centerpoints_layer.currentLayer()
    
    # Setting the progress bar
    self.progressMessageBar = self.iface.messageBar().createMessage('Generating Obstacle File...')
    self.progressBar = QProgressBar()
    self.progressBar.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
    self.progressMessageBar.layout().addWidget(self.progressBar)
    self.iface.messageBar().pushWidget(self.progressMessageBar, Qgis.Info)
    self.progressBar.setRange(0, 100)
    

    #*Output directory and filename (the script will put a number after the filename if there are more then one center point)*
    outputDirectory = str(_mainDialog.outdir.text())
    # outputFileName = str(_mainDialog.outfile.text())
    # outputDirectory = _mainDialog.outdirFile.GetDirectory
    outputFileName = "".join(outputDirectory.split(path.sep)[-1:])
    outDirOnly = outputDirectory[0:-1 * len(outputFileName)]
    if not path.exists(outDirOnly):
      self.progressBar.setValue(100)
      self.progressMessageBar.setText(outDirOnly + ' is not a valid directory!')
      return
    
    if (buildingsLayer is not None):
      # only proceed of a height field is selected
      if (self.buildLayerIs3d or (_mainDialog.Sel_buildhight_field.currentIndex() >= 0)):
        
        # read building geometries
        buildingGeometries = self.extractPolygonGeometries(buildingsLayer)
        
        # read buildings height field if not a 3d layer
        if self.buildLayerIs3d:
           buildingElevationField = ''
        else:
           buildingElevationFieldName = _mainDialog.Sel_buildhight_field.currentField()
           buildingElevationField = self.extractAttributeByName(buildingsLayer, buildingElevationFieldName)
        
        # prepare progress
        numBuildingGeometries = len(buildingGeometries)
        progressPercent = 0
        self.progressBar.setValue(progressPercent)
        self.progressMessageBar.setText('Generating Obstacle File...')
        
      else:
        QMessageBox.critical(None, "Conversion could not be performed", str("Invalid field index: = " + str(_mainDialog.Sel_buildhight_field.currentField() + ". Please select a building hight field!")))
    
    
    # read trees if any
    if (treesLayer is not None):                                                             # if contains Trees
      treesGeometryField = self.extractPointCoordinates(treesLayer)
      if(treesGeometryField[1] == True):
        return
      treesGeometryField = treesGeometryField[0]
      treeHeightField = self.extractAttributeByName(treesLayer, _mainDialog.Sel_treehight_field.currentField())
      crownExpansionField = self.extractAttributeByName(treesLayer, _mainDialog.Sel_crownexp_field.currentField())
      trunkHeightField = self.extractAttributeByName(treesLayer, _mainDialog.Sel_trunchight_field.currentField())
      trunkRadiusField = self.extractAttributeByName(treesLayer, _mainDialog.Sel_truncrad_field.currentField())
      treeTypeField = self.extractAttributeByName(treesLayer, _mainDialog.Sel_treetype_field.currentField())
      
    
    # check if center points layer specified
    if(centerPointsLayer is not None):
      
      # get field name for center points elevation offset
      centerPointElevationFieldName  = _mainDialog.Sel_centerpoints_field.currentField()
      
      # get elevation offset values
      centerPointElevationField  = self.extractAttributeByName(centerPointsLayer, centerPointElevationFieldName)
      
      # get the center poiunt geometries (must be points)
      centerPointGeometries = self.extractPointCoordinates(centerPointsLayer)  # geometry (x, y, coordinate)
      
      
      # fail if the extraction of center point geometries did not work
      if(centerPointGeometries[1] == True):
        return
      
      # keep the list of points only
      centerPointGeometry = centerPointGeometries[0]
      
    # if no layer present set center of map canvas as center point
    else:
      centerPointElevationField = []
      centerPointElevationField.append(0.0)  # use offset of 0m if not set
      centerPointGeometry = []
      centerPointGeometry.append(self.canvas.extent().center())  # returns center point, tested on console, works
    
    # number of center points specified (at least one equaling the center of the scene)
    numCenterPoints = len(centerPointGeometry)
    
    # pre-calculate the maximum value for the progress bar calculation
    maxProgress = numCenterPoints * numBuildingGeometries  # trees are fast, ignore those
    
    # crreate an obs file for every center point specified
    for i in range(numCenterPoints):
      
      # get the current center point geometry
      curCenterPointGeometry = centerPointGeometry[i]  # actual center point
      
      # get center point's x and y coordinate, as well as the elevation offset
      cx = curCenterPointGeometry.x()
      cy = curCenterPointGeometry.y()
      cz = centerPointElevationField[i]   # elevation (z-dir)
      
      # generate output file name and csv file writer
      outputFileNameAndDirectory = outputDirectory
      
      # remove the file extention if present
      if outputDirectory[-4:] == '.obs':
          outputFileNameAndDirectory = outputDirectory[:-4]
      
      # add a count index number if there is more than one center point
      if numCenterPoints > 1:
        outputFileNameAndDirectory = outputFileNameAndDirectory + str(i)
      
      # add the file extention '.obs'
      outputFileNameAndDirectory = outputFileNameAndDirectory + ".obs"
      
      # the columns in an obs file are seperated by a blank
      fieldDelimiter = " "
      
      # try to open the file with write permission. This might fail due to several reasons
      try:        # therefore try/except block required for all file operation
        outputFile = open(outputFileNameAndDirectory, 'w')
      except:
        QMessageBox.critical(None, "Error", str("Failure opening " + outputFileNameAndDirectory))
      
      # if file is accessible, configure file writer to write obs file
      writer = csv.writer(outputFile, delimiter = fieldDelimiter, quotechar = '#', lineterminator = '\n')
      
      # write header row, that will be ignored by RayMan
      writer.writerow([" RayMan Pro obstacle file "]) # write header
      
      # parse buildings if there are any present
      for b in range(numBuildingGeometries):  # iterate over buildings
        
        # calculate progress
        progressPercent = (((i * numBuildingGeometries) + b) / maxProgress) * 100
        
        # show progess in message bar
        self.progressBar.setValue(progressPercent)
        self.progressMessageBar.setText('Generating Obstacle File...')
        
        # get current geometry from buildings layer
        buildingGeometry = buildingGeometries[b]  # extract building number a
        
        # get the horizontal area of the current geometry to test for vertical polygons
        geometryArea = buildingGeometry.area()  # returning area 
        
        # ignore every polygon of area smaller than 0.05 m^2. We are dealing with buildings here!
        if(round(geometryArea, 2) > 0.05):
          
          # do Delaunay triangulation to split the polygons in a fast and simple way
          triangles = buildingGeometry.delaunayTriangulation(0.01, False)
          
          # convert the result to a GeometryCollection to facilitate extraction of individual items
          triangles = triangles.asGeometryCollection()
          
          # iterate over all individual triangles 't' in the geometryCollection 'triangles'
          for t in triangles:
             # all t are Polygons with 4 points while 1st and last point are the same
             
             # test if polygon is outside original polygon (might happen for multi polygons)
             if (buildingGeometry.contains(t)): # if the original polygon contains t
               # if (t.within(buildingGeometry)):
               
               # extract the individual vertices as a list
               geometryVertices = self.extractVerticesFromPolygon(t)
               
               # make sure the list is not empty
               if (geometryVertices == []):
                 continue
               
               # only use the first three points, as the 4th equals the first to close the polygon ring
               point1 = geometryVertices[0]  # extract the three points
               point2 = geometryVertices[1]
               point3 = geometryVertices[2]
               
               # get the height of the building
               roofHeight = None
               if not self.buildLayerIs3d:
                 roofHeight = buildingElevationField[b]
               
               # create a new line for the obs file
               line = self.pointsToBuildingObstacleRow(point1, point2, point3, None, roofHeight, cx, cy, cz)
               
               # write the row created above to the file
               writer.writerow(line)
               
      # parse trees if there is a tree layer specified
      if (treesLayer is not None):
        
        # and it contains geometries, that yould be extracted
        if (len(treesGeometryField) == len(treeHeightField)):  # to catch an error if the extraction fails
          
          # get all the attributes required to form a tree
          for t in range(len(treeHeightField)):     # iterate over attribute table of tree layer
            treeGeometry   = treesGeometryField[t]  # Geometry (coordinates)
            treeHeight     = treeHeightField[t] - cz     # Parameters
            crownExpansion = crownExpansionField[t]
            trunkHeight    = trunkHeightField[t] - cz
            trunkRadius    = trunkRadiusField[t]
            treeType       = treeTypeField[t]       # Type of tree
            treePosX       = treeGeometry.x() - cx   # calculate relative coordinates
            treePosY       = treeGeometry.y() - cy
            writer.writerow([treeType, str(treePosX), str(treePosY), str(treeHeight), str(crownExpansion), str(trunkHeight), str(trunkRadius), "0.30", "0.95"])
            
        # if there were no points extracted (something definetly went wrong)
        else:
          QMessageBox.critical(None, "Error converting trees", str(str(len(treesGeometryField)) + " geometries for " + str(len(treeHeightField)) + "trees."))
      
      # stop writing the file by closing the file writer
      del writer  # close file
    
    # Print a success message in the message bar
    self.progressBar.setValue(100)
    self.progressMessageBar.setText("The obstacle files are ready! :)")
    
  
  
  ### create Buildings and/or Tree layer from obstacle file!
  def generateObsLayers(self):
      obsFileName = _mainDialog.obsFileNameField.filePath()  # this for some reason returns the file name as well!
      if obsFileName == '':
          QMessageBox.critical(None, "No file selected", "Please select an obstacle file to be loaded!")
          return
      layerProjection = _mainDialog.projectionSettingsBox.crs().authid()
      centerPointLat = _mainDialog.latBox.value()
      centerPointLon = _mainDialog.lonBox.value()
      
      try:                                     # Try/Except block for file operation
          obsFile = open(obsFileName, 'r')  # set nonsense quotechar to avoid unintended strings
      except:
          QMessageBox.critical(None, "Bad filename", "File Path or Name is invalid!")
          return
      fieldDelimiter = " "  # set delimiter to blank
      reader = csv.reader(obsFile, delimiter = fieldDelimiter, quotechar = '"', lineterminator = '\n')
      
      buildingsGeom = []
      buildHeight = []
      buildAlbedo = []
      buildEmissivity = []
      treesGeom = []
      treeHeight = []
      crownExpansion = []
      trunkHeight = []
      trunkRadius = []
      treeType = []
      treeAlbedo = []
      treeEmissivity = []
      for row in reader:
          if len(row) == 0 or row[0] == "#":
              continue  # skip empty or commented rows (e.g. header)
          if len(row) >= 25 and (row[0] == "g"):
              point1 = QgsPoint(float(row[1]) + centerPointLat, float(row[2]) + centerPointLon, float(row[3]))
              point2 = QgsPoint(float(row[4]) + centerPointLat, float(row[5]) + centerPointLon, float(row[6]))
              point3 = QgsPoint(float(row[7]) + centerPointLat, float(row[8]) + centerPointLon, float(row[9]))
              point4 = QgsPoint(float(row[10]) + centerPointLat, float(row[11]) + centerPointLon, float(row[12]))
              buildHeight.append(max([float(row[3]), float(row[6]), float(row[9]), float(row[12])]))
              if len(row) >= 26:  # if "pro" file
                  buildAlbedo.append(float(row[25]))
                  buildEmissivity.append(float(row[26]))
              else:
                  buildAlbedo.append(0.3)
                  buildEmissivity.append(0.95)
              
              if point1 != point4:
                  # geom = QgsGeometry.fromPolygon( [[point1, point2, point3, point4, point1]] )  # 2D only
                  wktString = "PolygonZ ((" + str(point1.x()) + " " + str(point1.y()) + " " + str(point1.z()) + ", " + \
                    str(point2.x()) + " " + str(point2.y()) + " " + str(point2.z()) + ", " + \
                    str(point3.x()) + " " + str(point3.y()) + " " + str(point3.z()) + ", " + \
                    str(point4.x()) + " " + str(point4.y()) + " " + str(point4.z()) + ", " + \
                    str(point1.x()) + " " + str(point1.y()) + " " + str(point1.z()) + "))"
                  
                  geom = QgsGeometry.fromWkt(wktString)
                  if(round(geom.area(), 2) > 0.05):
                      buildingsGeom.append(geom)
                  else:
                      continue
              else:
                  # geom = QgsGeometry.fromPolygon( [[ point1, point2, point3, point1 ]] )  # 2D onbly
                  wktString = "PolygonZ ((" + str(point1.x()) + " " + str(point1.y()) + " " + str(point1.z()) + ", " + \
                    str(point2.x()) + " " + str(point2.y()) + " " + str(point2.z()) + ", " + \
                    str(point3.x()) + " " + str(point3.y()) + " " + str(point3.z()) + ", " + \
                    str(point1.x()) + " " + str(point1.y()) + " " + str(point1.z()) + "))"
                  
                  geom = QgsGeometry.fromWkt(wktString)
                  if(round(geom.area(), 2) > 0.05):
                      buildingsGeom.append(geom)
                  else:
                      continue
              
          if len(row) >= 7 and(row[0] == "n" or row[0] == "l"):
              # [treeType, str(treePosX), str(treePosY), str(treeHeight), str(crownExpansion), str(trunkHeight), str(trunkRadius), "0.30", "0.95"]
              treeType.append(row[0])
              point1 = QgsPointXY(float(row[1]) + centerPointLat, float(row[2]) + centerPointLon)
              treesGeom.append(QgsGeometry.fromPointXY( point1 ))
              treeHeight.append(float(row[3]))
              crownExpansion.append(float(row[4]))
              trunkHeight.append(float(row[5]))
              trunkRadius.append(float(row[6]))
              if len(row) >= 9:
                  treeAlbedo.append(float(row[7]))
                  treeEmissivity.append(float(row[8]))
              else:
                  treeAlbedo.append(0.3)
                  treeEmissivity.append(0.95)
      
      # create layers
      if len(buildingsGeom) > 0:
        buildingsLayer = QgsVectorLayer("Polygon?crs=" + layerProjection, obsFileName + "_Buildings", "memory")
        dataProvider = buildingsLayer.dataProvider()
        buildingsLayer.startEditing()
        dataProvider.addAttributes([QgsField("roofHeight", QVariant.Double), 
                                    QgsField("Albedo", QVariant.Double), 
                                    QgsField("Emissivity", QVariant.Double)])
        
        for i in range(len(buildingsGeom)):
            feature = QgsFeature()
            feature.setGeometry(buildingsGeom[i])
            feature.setAttributes([buildHeight[i], buildAlbedo[i], buildEmissivity[i]])
            dataProvider.addFeatures( [feature] )
        buildingsLayer.commitChanges()  # write layer
        #buildingsLayer.stopEditiong()
        QgsProject.instance().addMapLayer(buildingsLayer)
      
      if len(treesGeom) > 0:
        treesLayer = QgsVectorLayer("Point?crs=" + layerProjection, obsFileName + "_Trees", "memory")
        dataProvider = treesLayer.dataProvider()
        treesLayer.startEditing()
        dataProvider.addAttributes([QgsField("treeType", QVariant.String), 
                                    QgsField("treeHeight", QVariant.Double), 
                                    QgsField("crownRadius", QVariant.Double), 
                                    QgsField("trunkHeight", QVariant.Double), 
                                    QgsField("trunkDiameter", QVariant.Double), 
                                    QgsField("Albedo", QVariant.Double), 
                                    QgsField("Emissivity", QVariant.Double)])
        
        for i in range(len(treesGeom)):
            feature = QgsFeature()
            feature.setGeometry(treesGeom[i])
            fet_albedo = 0.3
            feature.setAttributes( [ treeType[i], 
                                     treeHeight[i],
                                     crownExpansion[i],
                                     trunkHeight[i],
                                     trunkRadius[i],
                                     treeAlbedo[i],
                                     treeEmissivity[i] ] )
            dataProvider.addFeatures( [feature] )
        treesLayer.commitChanges()  # write layer
        #treesLayer.stopEditing()
        QgsProject.instance().addMapLayer(treesLayer)
      
  
  
  def mainConfigDialog(self): 
        
      global _mainDialog 
      _mainDialog = ShowDialog2()
      
      # global canvas
      _welcomeDialogue.close()
      
      # buildings layer GUI handeling
      _mainDialog.Sel_Build_Layer.setFilters(QgsMapLayerProxyModel.PolygonLayer)
      _mainDialog.Sel_Build_Layer.setCurrentIndex(0) # Set selected item to first item (to avoid the -1st Layer being selected if nothing is changed
      self.buildLayerIndexChanged()  # populate building layer fields dropdown
      _mainDialog.Sel_buildhight_field.setFilters(QgsFieldProxyModel.Numeric)
      _mainDialog.use3dCheckBox.stateChanged.connect(self.buildLayer3dCheckboxChecked)
      
      # tree layer GUI handeling
      noTreeLayer = []
      noTreeLayer.append('-')
      treeComboSize = _mainDialog.Sel_Tree_Layer.count()
      _mainDialog.Sel_Tree_Layer.setFilters(QgsMapLayerProxyModel.PointLayer)
      _mainDialog.Sel_Tree_Layer.setAdditionalItems(noTreeLayer)  # Add layer names to "Select Trees Layer" dropdown menu
      _mainDialog.Sel_Tree_Layer.setCurrentIndex(treeComboSize) # Set selected item to first item (to avoid the -1st Layer being selected if nothing is changed
      _mainDialog.Sel_treehight_field.setFilters(QgsFieldProxyModel.Numeric)
      _mainDialog.Sel_crownexp_field.setFilters(QgsFieldProxyModel.Numeric)
      _mainDialog.Sel_treetype_field.setFilters(QgsFieldProxyModel.String)
      _mainDialog.Sel_trunchight_field.setFilters(QgsFieldProxyModel.Numeric)
      _mainDialog.Sel_truncrad_field.setFilters(QgsFieldProxyModel.Numeric)
      
      # center point layer GUI handeling
      centerComboSize = _mainDialog.Sel_centerpoints_layer.count()
      _mainDialog.Sel_centerpoints_layer.setFilters(QgsMapLayerProxyModel.PointLayer)
      _mainDialog.Sel_centerpoints_layer.setAdditionalItems(noTreeLayer)
      _mainDialog.Sel_centerpoints_layer.setCurrentIndex(centerComboSize) # Set selected item to first item (to avoid the -1st Layer being selected if nothing is changed
      _mainDialog.Sel_centerpoints_field.setFilters(QgsFieldProxyModel.Numeric)
      
      # output file GUI handeling
      currentprofile = self.getProjectFilePathAndName() 
      currentprofile_dir = currentprofile[0]
      currentprofile_file = currentprofile[1]
      _mainDialog.outdir.setText(path.join(currentprofile_dir, currentprofile_file))
      _mainDialog.selectOutDirButton.clicked.connect(self.on_selectOutFileButtonButton_clicked)
      # _mainDialog.outfile.setText( currentprofile_file )
      _mainDialog.outfile.setVisible(False)  # not used any more
      _mainDialog.outfilelabel.setVisible(False)
      _mainDialog.outdirFile.setDialogTitle = "Select .obs output file name"
      _mainDialog.outdirFile.SaveFile = True
      if len(currentprofile_dir) > 0:
          _mainDialog.outdirFile.setDefaultRoot = currentprofile_dir
      _mainDialog.outdirFile.setFilter = 'obs'
      _mainDialog.outdirFile.setEnabled(False)
      _mainDialog.outdirFile.setVisible(False)  # disable until create file mode is fixed by Qgs project!
      _mainDialog.latBox.setValue(self.canvas.extent().center().x())  # 3413905.50)  # replace by proper position
      _mainDialog.lonBox.setValue(self.canvas.extent().center().y())  # 5318046.40)
      _mainDialog.projectionSettingsBox.setCrs(self.iface.activeLayer().crs())
            
      # Set connections for buttons and dropdown signals
      _mainDialog.Cancel.clicked.connect(_mainDialog.close)
      _mainDialog.Help.clicked.connect(self.shwhelp)
      _mainDialog.Convert.clicked.connect(self.generateObstacleFile)
      _mainDialog.Sel_Build_Layer.activated[int].connect(self.buildLayerIndexChanged)
      _mainDialog.Sel_Tree_Layer.activated[int].connect(self.treeLayerIndexChanged)
      _mainDialog.Sel_centerpoints_layer.activated[int].connect(self.centerpointsLayerIndexChanged)
      _mainDialog.Tabs.currentChanged.connect(self.guiTabIndexChanged)
      
      # _mainDialog.AddObsAsLayerButton.clicked.connect(self.generateObsLayers)
      _mainDialog.AddObsAsLayerButton.setVisible(False)
      
      _mainDialog.show() # Show second dialog
      _mainDialog.exec_()
      


  # show welcome dialog
  def run(self): 
    # create and show the dialog 
    global _welcomeDialogue
    _welcomeDialogue = ShowDialog1()
    # QtCore.QObject.connect(_welcomeDialogue.Cancel1, QtCore.SIGNAL("clicked()"), _welcomeDialogue.close)  # depr!
    # QtCore.QObject.connect(_welcomeDialogue.Next1, QtCore.SIGNAL("clicked()"), self.mainConfigDialog)  # depr! 
    _welcomeDialogue.Cancel1.clicked.connect(_welcomeDialogue.close)
    _welcomeDialogue.Next1.clicked.connect(self.mainConfigDialog) 
    # show the dialog
    _welcomeDialogue.show()
    _welcomeDialogue.exec_() 
    
