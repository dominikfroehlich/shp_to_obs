"""
/***************************************************************************
Name			 	 : shptoobs
Description          : Creates a RayMan input file from shp. 
Date                 : 20/Jul/12 
copyright            : (C) 2012 by Tamas Gal, Dominik Froehlich
email                : dominik_froe@web.de 
 ***************************************************************************/
"""
from __future__ import absolute_import

from qgis.PyQt import QtCore, QtGui 
# from Ui_shp_to_obs import Ui_shp_to_obs
from .shpobs_Step1 import Ui_shpobsStep1
from .shpobs_Step2 import Ui_shpobsStep2
from LoadFiles import *
# create the dialog for shp_to_obs
class shp_to_obsDialog(QtGui.QDialog):
  def __init__(self): 
    QtGui.QDialog.__init__(self) 
    # Set up the user interface from Designer. 
    self.ui = Ui_shp_to_obs ()
    self.ui.setupUi(self)
    self.processStatus = True # To handle a dialog cancel event
    
