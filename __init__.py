# -*- coding: utf-8 -*-
"""
/***************************************************************************
Name			 	 : shptoobs
Description          : Creates a RayMan input file from shp. 
Date                 : 21/Jun/2018
copyright            : (C) 2018 by Tamas Gal, Dominik Froehlich
email                : dominik.froehlich@meteo.uni-freiburg.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface): 
  # load shp_to_obs class from file shp_to_obs
  from .shp_to_obs import shp_to_obs 
  return shp_to_obs(iface)
