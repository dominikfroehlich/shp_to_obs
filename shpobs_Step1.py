# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'shpobs_Step1.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_shpobsStep1(object):
    def setupUi(self, shpobsStep1):
        shpobsStep1.setObjectName("shpobsStep1")
        shpobsStep1.resize(536, 356)
        shpobsStep1.setWindowTitle("Shp to Obs Converter")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        shpobsStep1.setWindowIcon(icon)
        shpobsStep1.setAccessibleName("")
        self.Next1 = QtWidgets.QPushButton(shpobsStep1)
        self.Next1.setGeometry(QtCore.QRect(410, 310, 98, 27))
        self.Next1.setDefault(True)
        self.Next1.setObjectName("Next1")
        self.Cancel1 = QtWidgets.QPushButton(shpobsStep1)
        self.Cancel1.setGeometry(QtCore.QRect(310, 310, 98, 27))
        self.Cancel1.setObjectName("Cancel1")
        self.Introduction = QtWidgets.QLabel(shpobsStep1)
        self.Introduction.setGeometry(QtCore.QRect(40, 20, 461, 271))
        self.Introduction.setObjectName("Introduction")

        self.retranslateUi(shpobsStep1)
        QtCore.QMetaObject.connectSlotsByName(shpobsStep1)

    def retranslateUi(self, shpobsStep1):
        _translate = QtCore.QCoreApplication.translate
        self.Next1.setText(_translate("shpobsStep1", "Next"))
        self.Cancel1.setText(_translate("shpobsStep1", "Cancel"))
        self.Introduction.setText(_translate("shpobsStep1", "<html><head/><body><p align=\"center\"><span style=\" font-size:26pt; font-weight:600;\">Shp to Obs</span></p><p><span style=\" font-weight:600;\">Conversion of Shapefiles to RayMan / SkyHelios Object files.</span></p><p>Programming: Dominik Fröhlich, Tamás Gál, and Andreas Matzarakis</p><p>Contact: Dominik Fröhlich or Andreas Matzarakis<br/>Research Centre Human Biometeorology<br/>German Meteorological Service<br/>Stefan-Meier-Str. 4<br/>Tel: +49-69-8062-9596<br/>Fax: +49-69-8062-9622<br/>Email: dominik.froehlich@mailbox.org, or<br/>andreas.matzarakis@dwd.de </p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    shpobsStep1 = QtWidgets.QDialog()
    ui = Ui_shpobsStep1()
    ui.setupUi(shpobsStep1)
    shpobsStep1.show()
    sys.exit(app.exec_())

